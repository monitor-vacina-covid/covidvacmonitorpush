import { PushSubscription } from 'web-push';
import { Dose, Tipo } from '../interfaces/grupo.interface';

export class UserSubscription {
    public doses: Dose[];
    public tipos: Tipo[];
    public subscription: PushSubscription;

    get key() {
        return `${this.subscription.keys?.p256dh}${this.subscription.keys?.auth}`
    }

    constructor(doses: Dose[], tipos: Tipo[], subscription: PushSubscription) {
        this.doses = doses;
        this.tipos = tipos;
        this.subscription = subscription;
    }
}