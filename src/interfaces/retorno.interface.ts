import { LocalGrupo } from "./localGrupo.interface";

export interface Retorno {
    sucesso: boolean;
    userErro?: {code?: any, mensagem?: string},
    logErro?: any,
    data: LocalGrupo[]
}